import MockDate from "mockdate";
import { render, screen } from "@testing-library/react";
import Greeting from "../greeting";

describe("Testing Greeting Component Functionalities", () => {
  test("Test 1 - Current Time - 08:30 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 08:30");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Morning");
  });

  test("Test 2 - Current Time - 13:30 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 13:30");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Afternoon");
  });

  test("Test 3 - Current Time - 18:30 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 18:30");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Evening");
  });

  test("Test 4 - Current Time - 22:30 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 22:30");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Night");
  });

  // Exceptional Testing for Margins
  test("Test 5 - Current Time - 00:00 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 00:00");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Morning");
  });

  test("Test 6 - Current Time - 12:00 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 12:00");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Morning");
  });

  test("Test 7 - Current Time - 17:00 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 17:00");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Evening");
  });

  test("Test 8 - Current Time - 24:00 H ", () => {
    render(
      <Greeting
        mockTime={() => {
          MockDate.set("2022-07-08 24:00");
        }}
      />
    );
    const greetingComp = screen.getByTestId("greeting-message");
    expect(greetingComp).toBeInTheDocument();
    expect(greetingComp).toHaveTextContent("Good Morning");
  });
});
