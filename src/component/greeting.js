const Greeting = ({ mockTime }) => {
  const getCurrentGreeting = () => {
    mockTime()
    const date = new Date();
    const hours = date.getHours();
    switch (true) {
      case hours >= 0 && hours <= 12:
        return "Good Morning";
      case hours >= 12 && hours <= 16:
        return "Good Afternoon";
      case hours >= 16 && hours <= 21:
        return "Good Evening";
      case hours >= 21 && hours <= 24:
        return "Good Night";
      default:
        return "Undefined";
    }
  };
  return <div data-testid="greeting-message">{getCurrentGreeting()}</div>;
};

export default Greeting;
