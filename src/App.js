import MockDate from 'mockdate'
import Greeting from "./component/greeting"

const App = () => {
  return <div><Greeting mockTime={() => {MockDate.set("2022-07-08 13:30")}} /></div>;
};

export default App;
